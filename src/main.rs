const APP_ICON: &[u8] = include_bytes!("../fa1.ico");
fn main() {
    let icon = image::load_from_memory(APP_ICON).unwrap();
    let native_options = eframe::NativeOptions{
        default_theme:eframe::Theme::Light,
        icon_data:Some(eframe::IconData{
            width: icon.width(),
            height: icon.height(),
            rgba: icon.into_bytes(),
        }),
        ..Default::default()
    };
    eframe::run_native("MyApp", native_options, Box::new(|cc| Box::new(MyEguiApp::new(cc))));
}

#[derive(Default)]
#[derive(serde::Serialize,serde::Deserialize)]
struct MyEguiApp {}

impl MyEguiApp {
    fn new(cc: &eframe::CreationContext<'_>) -> Self {
        // Customize egui here with cc.egui_ctx.set_fonts and cc.egui_ctx.set_visuals.
        // Restore app state using cc.storage (requires the "persistence" feature).
        // Use the cc.gl (a glow::Context) to create graphics shaders and buffers that you can use
        // for e.g. egui::PaintCallback.
        Self::font_init(cc);
        if let Some(storage) = cc.storage {
            if let Some(data) = eframe::get_value(storage, eframe::APP_KEY){
                return data;
            }
        }
        Self::default()
    }
    fn font_init(cc: &eframe::CreationContext<'_>){
        let mut fonts = eframe::egui::FontDefinitions::default();

            // Install my own font (maybe supporting non-latin characters):
        fonts.font_data.insert("my_font".to_owned(),
        eframe::egui::FontData::from_static(include_bytes!("../SmileySans-Oblique.ttf"))); // .ttf and .otf supported

        // Put my font first (highest priority):
        fonts.families.get_mut(&eframe::egui::FontFamily::Proportional).unwrap()
            .insert(0, "my_font".to_owned());

        // Put my font as last fallback for monospace:
        fonts.families.get_mut(&eframe::egui::FontFamily::Monospace).unwrap()
            .push("my_font".to_owned());

        cc.egui_ctx.set_fonts(fonts);
        use eframe::egui::FontFamily::Monospace;

        let mut style = (*cc.egui_ctx.style()).clone();
        style.text_styles = [
            (
                eframe::egui::TextStyle::Heading,
                eframe::egui::FontId::new(14.0, Monospace),
            ),
            (
                eframe::egui::TextStyle::Body,
                eframe::egui::FontId::new(14.0, Monospace),
            ),
            (
                eframe::egui::TextStyle::Monospace,
                eframe::egui::FontId::new(14.0, Monospace),
            ),
            (
                eframe::egui::TextStyle::Button,
                eframe::egui::FontId::new(14.0, Monospace),
            ),
            (
                eframe::egui::TextStyle::Small,
                eframe::egui::FontId::new(14.0, Monospace),
            ),
        ]
        .into();
        cc.egui_ctx.set_style(style);
    }
}

impl eframe::App for MyEguiApp {
   fn update(&mut self, ctx: &eframe::egui::Context, frame: &mut eframe::Frame) {
       eframe::egui::CentralPanel::default().show(ctx, |ui| {
           ui.heading("Hello World!");
           
       });
   }
}